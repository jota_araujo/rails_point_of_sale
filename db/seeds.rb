# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

require 'faker'

# Faker::Config.locale = 'pt_BR'



Fabricator(:user) do
   name { Faker::Name.name }
   email { Faker::Internet.email }
   password "12345678"
end



Fabricator(:brand_group) do
	name
end	

Fabricator(:product_group) do
	name
end	

Fabricator(:brand) do
	name
	group
end	

Fabricator(:product) do
	transient :brands
	name { "Pacote Ração #{Fabricate.sequence(1)*2} Kg" }
	brand {|attrs| attrs[:brands].sample }
	group
	unit "embalagem"
	is_service false
	price_cents 1500
	cost_cents 1000
	margin_cents 0
	commission 0
	track_stock false
	stock_amount 5
	status 0
end

Fabricator(:account_type) do
	name
end	

Fabricator(:account) do
	transient :users
	operator { |attrs| attrs[:users].pop }
	name { |attrs| "Caixa - #{attrs[:operator].name}" }
	balance_cents 1000
	status 0
end	

b_group = Fabricate(:brand_group, name:"Rações")
brand1 = Fabricate(:brand, name: "Whiskas", group: b_group)
brand2 = Fabricate(:brand, name: "SuperPet", group: b_group)
p_group = Fabricate(:product_group, name:"Rações")

puts 'creating 10 users...'
users = Fabricate.times(10, :user)
puts 'finished creating users'

puts 'creating 10 products...'
products = Fabricate.times(10, :product, group: p_group, brands: [brand1, brand2])
puts 'finished creating products'

cash = Fabricate(:account_type, name: 'Dinheiro')
check = Fabricate(:account_type, name: 'Cheque')
#visa = Fabricate(:account_type, name: 'Cartão de Crédito VISA')
#mastercard = Fabricate(:account_type, name: 'Cartão de Crédito Mastercard')

puts 'creating 10 accounts...'
Fabricate.times(10, :account, type:cash, users: users)
puts 'finished creating accounts'


