# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160526040040) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "account_types", force: :cascade do |t|
    t.string "name", null: false
  end

  create_table "accounts", force: :cascade do |t|
    t.integer  "type_id"
    t.integer  "operator_id"
    t.string   "name",                      null: false
    t.integer  "balance_cents",             null: false
    t.integer  "status",        default: 0, null: false
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.index ["operator_id"], name: "index_accounts_on_operator_id", using: :btree
    t.index ["type_id"], name: "index_accounts_on_type_id", using: :btree
  end

  create_table "brand_groups", force: :cascade do |t|
    t.string "name", null: false
  end

  create_table "brands", force: :cascade do |t|
    t.string  "name",     null: false
    t.integer "group_id"
    t.index ["group_id"], name: "index_brands_on_group_id", using: :btree
  end

  create_table "dailies", force: :cascade do |t|
    t.datetime "last_closed_at"
    t.datetime "last_opened_at"
    t.integer  "account_id"
    t.integer  "balance_cents",  null: false
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["account_id"], name: "index_dailies_on_account_id", using: :btree
  end

  create_table "line_items", force: :cascade do |t|
    t.integer  "sale_id",                      null: false
    t.integer  "item_id",                      null: false
    t.integer  "provider_id"
    t.integer  "unit_price_cents",             null: false
    t.integer  "qty",                          null: false
    t.integer  "total_cents",                  null: false
    t.integer  "discount_percent", default: 0, null: false
    t.integer  "tax_percent",      default: 0, null: false
    t.string   "meta"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.index ["item_id"], name: "index_line_items_on_item_id", using: :btree
    t.index ["provider_id"], name: "index_line_items_on_provider_id", using: :btree
    t.index ["sale_id"], name: "index_line_items_on_sale_id", using: :btree
  end

  create_table "operation_groups", force: :cascade do |t|
    t.string "name", null: false
  end

  create_table "operation_types", force: :cascade do |t|
    t.string  "name",      null: false
    t.integer "group_id"
    t.boolean "is_credit", null: false
    t.index ["group_id"], name: "index_operation_types_on_group_id", using: :btree
  end

  create_table "operations", force: :cascade do |t|
    t.integer  "credit_account_id", null: false
    t.integer  "debit_account_id",  null: false
    t.integer  "credit_cents"
    t.integer  "debit_cents"
    t.integer  "discount_cents"
    t.integer  "tax_cents"
    t.integer  "type_id",           null: false
    t.integer  "payment_type_id",   null: false
    t.integer  "creator_id",        null: false
    t.integer  "sale_id"
    t.string   "description"
    t.string   "meta"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.index ["creator_id"], name: "index_operations_on_creator_id", using: :btree
    t.index ["credit_account_id"], name: "index_operations_on_credit_account_id", using: :btree
    t.index ["debit_account_id"], name: "index_operations_on_debit_account_id", using: :btree
    t.index ["payment_type_id"], name: "index_operations_on_payment_type_id", using: :btree
    t.index ["sale_id"], name: "index_operations_on_sale_id", using: :btree
    t.index ["type_id"], name: "index_operations_on_type_id", using: :btree
  end

  create_table "payment_types", force: :cascade do |t|
    t.string "name", null: false
  end

  create_table "product_groups", force: :cascade do |t|
    t.string "name", null: false
  end

  create_table "products", force: :cascade do |t|
    t.integer  "brand_id"
    t.integer  "group_id"
    t.boolean  "is_service",                   null: false
    t.string   "name",                         null: false
    t.string   "code"
    t.string   "unit",                         null: false
    t.string   "barcode"
    t.integer  "cost_cents"
    t.integer  "price_cents",                  null: false
    t.integer  "margin_cents"
    t.integer  "commission",   default: 0,     null: false
    t.integer  "status",                       null: false
    t.string   "meta"
    t.datetime "expiration"
    t.boolean  "track_stock",  default: false, null: false
    t.integer  "stock_min"
    t.integer  "stock_max"
    t.integer  "stock_amount",                 null: false
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.index ["brand_id"], name: "index_products_on_brand_id", using: :btree
    t.index ["group_id"], name: "index_products_on_group_id", using: :btree
  end

  create_table "sales", force: :cascade do |t|
    t.integer  "client_id",  null: false
    t.integer  "daily_id",   null: false
    t.integer  "status",     null: false
    t.string   "comment"
    t.string   "meta"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["client_id"], name: "index_sales_on_client_id", using: :btree
    t.index ["daily_id"], name: "index_sales_on_daily_id", using: :btree
    t.index ["status"], name: "index_sales_on_status", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "provider",               default: "email", null: false
    t.string   "uid",                    default: "",      null: false
    t.string   "encrypted_password",     default: "",      null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,       null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.string   "name"
    t.string   "nickname"
    t.string   "image"
    t.string   "email"
    t.json     "tokens"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["email"], name: "index_users_on_email", using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
    t.index ["uid", "provider"], name: "index_users_on_uid_and_provider", unique: true, using: :btree
  end

  add_foreign_key "accounts", "account_types", column: "type_id"
  add_foreign_key "accounts", "users", column: "operator_id"
  add_foreign_key "brands", "brand_groups", column: "group_id"
  add_foreign_key "dailies", "accounts"
  add_foreign_key "line_items", "products", column: "item_id"
  add_foreign_key "line_items", "sales"
  add_foreign_key "line_items", "users", column: "provider_id"
  add_foreign_key "operation_types", "operation_groups", column: "group_id"
  add_foreign_key "operations", "accounts", column: "credit_account_id"
  add_foreign_key "operations", "accounts", column: "debit_account_id"
  add_foreign_key "operations", "operation_types", column: "type_id"
  add_foreign_key "operations", "payment_types"
  add_foreign_key "operations", "sales"
  add_foreign_key "operations", "users", column: "creator_id"
  add_foreign_key "products", "brands"
  add_foreign_key "products", "product_groups", column: "group_id"
  add_foreign_key "sales", "dailies"
  add_foreign_key "sales", "users", column: "client_id"
end
