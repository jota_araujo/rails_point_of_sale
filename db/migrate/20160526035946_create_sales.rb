class CreateSales < ActiveRecord::Migration[5.0]
  def change
    create_table :sales do |t|
      t.integer :client_id, index: true, null: false
      t.integer :daily_id, index: true, null: false
      t.integer :status, index: true, null: false
      t.string :comment
      t.string :meta

      t.timestamps
    end
    add_foreign_key :sales , :users, column: :client_id
    add_foreign_key :sales , :dailies, column: :daily_id
  end
end
