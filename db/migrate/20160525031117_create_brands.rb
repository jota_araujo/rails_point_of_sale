class CreateBrands < ActiveRecord::Migration[5.0]
  def change
    create_table :brands do |t|
      t.string :name, null: false
      t.integer :group_id, index: true
    end
    add_foreign_key :brands , :brand_groups, column: :group_id
  end
end
