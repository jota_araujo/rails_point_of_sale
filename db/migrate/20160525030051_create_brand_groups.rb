class CreateBrandGroups < ActiveRecord::Migration[5.0]
  def change
    create_table :brand_groups do |t|
      t.string :name, null: false
    end
  end
end
