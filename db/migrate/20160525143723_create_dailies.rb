class CreateDailies < ActiveRecord::Migration[5.0]
  def change
    create_table :dailies do |t|
      t.datetime :last_closed_at
      t.datetime :last_opened_at
      t.integer  :account_id, index:true
      t.integer  :balance_cents, null: false

      t.timestamps
    end
    add_foreign_key :dailies , :accounts, column: :account_id
  end
end
