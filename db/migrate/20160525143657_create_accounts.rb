class CreateAccounts < ActiveRecord::Migration[5.0]
  def change
    create_table :accounts do |t|
      t.integer :type_id, index: true
      t.integer :operator_id, index: true
      t.string :name, null: false
      t.integer :balance_cents, null: false
      t.integer :status, null: false, default: 0

      t.timestamps
    end
    add_foreign_key :accounts , :account_types, column: :type_id
    add_foreign_key :accounts , :users, column: :operator_id
  end
end
