class CreateProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :products do |t|
      t.references :brand, foreign_key: true
      t.integer :group_id, index: true
      t.boolean :is_service, null: false
      t.string :name, null: false
      t.string :code, null: true 
      t.string :unit, null: false
      t.string :barcode
      t.integer :cost_cents
      t.integer :price_cents, null: false
      t.integer :margin_cents
      t.integer :commission, null: false, default: 0
      t.integer :status, null: false
      t.string :meta
      t.datetime :expiration
      t.boolean :track_stock, null: false, default: false
      t.integer :stock_min
      t.integer :stock_max
      t.integer :stock_amount, null: false

      t.timestamps
    end
    add_foreign_key :products , :product_groups, column: :group_id
  end
   
end
