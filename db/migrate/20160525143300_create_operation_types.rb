class CreateOperationTypes < ActiveRecord::Migration[5.0]
  def change
    create_table :operation_types do |t|
      t.string :name, null: false
      t.integer :group_id, index: true
      t.boolean :is_credit, null: false
    end
    add_foreign_key :operation_types , :operation_groups, column: :group_id
  end
end
