class CreateOperations < ActiveRecord::Migration[5.0]
  def change
    create_table :operations do |t|
      t.integer :credit_account_id, index: true, null: false
      t.integer :debit_account_id, index: true, null: false
      t.integer :credit_cents
      t.integer :debit_cents
      t.integer :discount_cents
      t.integer :tax_cents
      t.integer :type_id, index: true, null: false
      t.integer :payment_type_id, index: true, null: false
      t.integer :creator_id, index: true, null: false
      t.integer :sale_id, index: true
      t.string :description
      t.string :meta

      t.timestamps
    end
    add_foreign_key :operations , :accounts, column: :credit_account_id
    add_foreign_key :operations , :accounts, column: :debit_account_id
    add_foreign_key :operations , :operation_types, column: :type_id
    add_foreign_key :operations , :payment_types, column: :payment_type_id
    add_foreign_key :operations , :users, column: :creator_id
    add_foreign_key :operations , :sales, column: :sale_id
    
  end
end
