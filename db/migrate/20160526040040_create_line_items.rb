class CreateLineItems < ActiveRecord::Migration[5.0]
  def change
    create_table :line_items do |t|
      t.integer :sale_id, index:true, null: false
      t.integer :item_id, index:true, null: false
      t.integer :provider_id, index: true
      t.integer :unit_price_cents, null: false
      t.integer :qty, null: false
      t.integer :total_cents, null: false
      t.integer :discount_percent, null: false, default: 0
      t.integer :tax_percent, null: false, default: 0
      t.string :meta

      t.timestamps
    end
    add_foreign_key :line_items , :sales, column: :sale_id
    add_foreign_key :line_items , :products, column: :item_id
    add_foreign_key :line_items , :users, column: :provider_id

  end
end
