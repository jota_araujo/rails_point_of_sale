class Api::ProductsController < ApplicationController
  include SortHelpers

  before_action :set_product, only: [:show, :update, :destroy]

  def xss_options_request
    render :text => "hello"
  end

  # GET /products
  def index
    @products = Product.order(sanitized_ordering).page(params[:page]).per(8)
    render json: @products, meta: { pagination:
                                   { per_page: params[:per_page],
                                     total_pages: @products.total_pages,
                                     total_objects: @products.total_count } }

  end

  # GET /products/1
  def show
    render json: @product
  end

  # POST /products
  def create
    @product = Product.new(product_params)

    if @product.save
      render json: @product, status: :created, location: @product
    else
      render json: @product.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /products/1
  def update
    if @product.update(product_params)
      render json: @product
    else
      render json: @product.errors, status: :unprocessable_entity
    end
  end

  # DELETE /products/1
  def destroy
    @product.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Product.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def product_params
      params.require(:product).permit(:brand_id, :group_id, :is_service, :name, :unit, :barcode, :cost_cents, :price_cents, :margin_cents, :commission, :status, :meta, :expiration, :track_stock, :stock_amount)
    end
end
