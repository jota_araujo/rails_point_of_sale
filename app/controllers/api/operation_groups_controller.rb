class Api::OperationGroupsController < ApplicationController
  before_action :set_operation_group, only: [:show, :update, :destroy]

  # GET /operation_groups
  def index
    @operation_groups = OperationGroup.all

    render json: @operation_groups
  end

  # GET /operation_groups/1
  def show
    render json: @operation_group
  end

  # POST /operation_groups
  def create
    @operation_group = OperationGroup.new(operation_group_params)

    if @operation_group.save
      render json: @operation_group, status: :created, location: @operation_group
    else
      render json: @operation_group.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /operation_groups/1
  def update
    if @operation_group.update(operation_group_params)
      render json: @operation_group
    else
      render json: @operation_group.errors, status: :unprocessable_entity
    end
  end

  # DELETE /operation_groups/1
  def destroy
    @operation_group.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_operation_group
      @operation_group = OperationGroup.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def operation_group_params
      params.require(:operation_group).permit(:name)
    end
end
