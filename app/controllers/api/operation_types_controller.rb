class Api::OperationTypesController < ApplicationController
  before_action :set_operation_type, only: [:show, :update, :destroy]

  # GET /operation_types
  def index
    @operation_types = OperationType.all

    render json: @operation_types
  end

  # GET /operation_types/1
  def show
    render json: @operation_type
  end

  # POST /operation_types
  def create
    @operation_type = OperationType.new(operation_type_params)

    if @operation_type.save
      render json: @operation_type, status: :created, location: @operation_type
    else
      render json: @operation_type.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /operation_types/1
  def update
    if @operation_type.update(operation_type_params)
      render json: @operation_type
    else
      render json: @operation_type.errors, status: :unprocessable_entity
    end
  end

  # DELETE /operation_types/1
  def destroy
    @operation_type.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_operation_type
      @operation_type = OperationType.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def operation_type_params
      params.require(:operation_type).permit(:name, :group_id, :is_credit)
    end
end
