class Api::BrandGroupsController < ApplicationController
  before_action :set_brand_group, only: [:show, :update, :destroy]

  # GET /brand_groups
  def index
    @brand_groups = BrandGroup.all

    render json: @brand_groups
  end

  # GET /brand_groups/1
  def show
    render json: @brand_group
  end

  # POST /brand_groups
  def create
    @brand_group = BrandGroup.new(brand_group_params)

    if @brand_group.save
      render json: @brand_group, status: :created, location: @brand_group
    else
      render json: @brand_group.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /brand_groups/1
  def update
    if @brand_group.update(brand_group_params)
      render json: @brand_group
    else
      render json: @brand_group.errors, status: :unprocessable_entity
    end
  end

  # DELETE /brand_groups/1
  def destroy
    @brand_group.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_brand_group
      @brand_group = BrandGroup.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def brand_group_params
      params.require(:brand_group).permit(:name)
    end
end
