class ApplicationController < ActionController::API
  #include DeviseTokenAuth::Concerns::SetUserByToken

  #before_action :configure_permitted_parameters, if: :devise_controller?
  #before_action :clean_parameters

  before_filter :cors_preflight_check
  after_filter :cors_set_access_control_headers

  protected

  def clean_parameters
    params.delete :session
  end

  def configure_permitted_parameters
    added_attrs = [:username, :email, :password, :password_confirmation, :remember_me]
    devise_parameter_sanitizer.permit :sign_up, keys: added_attrs
    devise_parameter_sanitizer.permit :sign_in, keys: added_attrs
    devise_parameter_sanitizer.permit :account_update, keys: added_attrs
  end

   def cors_set_access_control_headers
    headers['Access-Control-Allow-Origin'] = '*'
    headers['Access-Control-Allow-Methods'] = 'PUT, PATCH, POST, GET, OPTIONS'
    headers['Access-Control-Max-Age'] = "1728000"
  end

  def cors_preflight_check
    headers['Access-Control-Allow-Origin'] = '*'
    headers['Access-Control-Allow-Methods'] = 'PUT, PATCH, POST, GET, OPTIONS'
    headers['Access-Control-Allow-Headers'] = 'X-Requested-With, X-Prototype-Version, Content-Type, Accept'
    headers['Access-Control-Max-Age'] = '1728000'
  end


  private

end
