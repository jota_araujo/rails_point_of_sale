class ProductGroupSerializer < ActiveModel::Serializer
  attributes :id, :name
end
