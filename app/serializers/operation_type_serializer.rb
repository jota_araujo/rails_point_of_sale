class OperationTypeSerializer < ActiveModel::Serializer
  attributes :id, :name, :is_credit
  has_one :group
end
