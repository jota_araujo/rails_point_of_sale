class LineItemSerializer < ActiveModel::Serializer
  attributes :id, :sale_id, :item_id, :provider_id, :unit_price_cents, :qty, :total_cents, :discount_percent, :tax_percent, :meta
end
