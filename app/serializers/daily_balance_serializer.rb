class DailyBalanceSerializer < ActiveModel::Serializer
  attributes :id, :daily_id, :category, :balance_cents
end
