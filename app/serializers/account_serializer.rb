class AccountSerializer < ActiveModel::Serializer
  attributes :id, :name, :balance_cents, :status
  has_one :type
  has_one :owner
end
