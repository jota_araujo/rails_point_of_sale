class OperationSerializer < ActiveModel::Serializer
  attributes :id, :credit_account_id, :debit_account_id, :credit_cents, :debit_cents, :discount_cents, :tax_cents, :type, :payment_type, :creator_id, :sale_id, :description, :meta
end
