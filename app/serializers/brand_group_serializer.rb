class BrandGroupSerializer < ActiveModel::Serializer
  attributes :id, :name
end
