class DailySerializer < ActiveModel::Serializer
  attributes :id, :last_closed_at, :last_opened_at
end
