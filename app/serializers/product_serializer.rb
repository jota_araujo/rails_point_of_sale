class ProductSerializer < ActiveModel::Serializer
  # attributes :id, :is_service, :name, :unit, :barcode, :cost_cents, :price_cents, :margin_cents, :commission, :status, :meta, :expiration, :track_stock, :stock_amount
   attributes :id, :name, :price_cents, :margin_cents, :commission, :expiration, :track_stock, :stock_amount
  has_one :brand
  has_one :group
end
