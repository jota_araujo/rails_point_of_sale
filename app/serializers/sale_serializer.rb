class SaleSerializer < ActiveModel::Serializer
  attributes :id, :client_id, :daily_id, :status, :comment, :meta
end
