class BrandSerializer < ActiveModel::Serializer
  attributes :id, :name
  has_one :group
end
