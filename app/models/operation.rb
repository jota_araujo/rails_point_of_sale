class Operation < ApplicationRecord
  has_one :credit_account, class_name: "Account"
  has_one :debit_account, class_name: "Account"
  belongs_to :type
  has_one    :payment_type

  validates :credit_account, :debit_account, :type , presence: true
end
