class Product < ApplicationRecord

  enum status: {
    active: 0,
    inactive: 1
  }

  belongs_to :brand
  belongs_to :group, class_name: "ProductGroup"

  
  validates :name, :unit, :price_cents, presence: true
end
