class Brand < ApplicationRecord
  belongs_to :group, class_name: "BrandGroup"
  has_many   :products


  validates :name, presence:true, uniqueness: true
  validates :group_id, presence:true
end
