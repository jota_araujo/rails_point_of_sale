class LineItem < ApplicationRecord
	belongs_to :sale
	belongs_to :item, class_name: "Product"

	validates :sale, :item, :unit_price, :qty, :total_cents, presence: true
end
