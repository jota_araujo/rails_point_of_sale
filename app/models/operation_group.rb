class OperationGroup < ApplicationRecord
	has_many :operation_types

	validates :name, presence:  true, uniqueness: true
end
