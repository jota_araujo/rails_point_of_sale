class Sale < ApplicationRecord

	enum status: {
	 open: 0,
	 closed: 1
	}

	belongs_to :daily
	has_one :client, class_name: "User"

	validates_presence_of :client
	validates_presence_of :daily
	validates_presence_of :status

	accepts_nested_attributes_for :line_items, allow_destroy: true
end
