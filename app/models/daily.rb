class Daily < ApplicationRecord
	has_many :operations
	has_one  :account

	validates :account_id, presence: true

end
