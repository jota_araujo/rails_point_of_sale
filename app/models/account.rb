class Account < ApplicationRecord
  belongs_to :type, class_name: "AccountType"
  belongs_to :operator, class_name: "User"
  has_many   :operations

  validates :name, presence:true, uniqueness: true
  validates :type, presence: true

end
