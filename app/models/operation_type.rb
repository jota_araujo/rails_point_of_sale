class OperationType < ApplicationRecord
  belongs_to :group, class_name:  "OperationGroup"
  has_many   :operations

  validates :name, presence:  true, uniqueness: true
end
