Rails.application.routes.draw do
	namespace :api do

	  resources :payment_types
	  resources :line_items
	  resources :operations
	  resources :sales
	  resources :daily_balances
	  resources :dailies
	  resources :accounts
	  resources :operation_types
	  resources :operation_groups
	  resources :account_types
	  resources :brand_groups
	  mount_devise_token_auth_for 'User', at: 'auth'
	  resources :products
	  resources :brands
	  resources :product_groups

	  match "*path", to: "products#xss_options_request", via: [:options]
	  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
	end  
end
