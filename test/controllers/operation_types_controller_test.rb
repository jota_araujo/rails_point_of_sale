require 'test_helper'

class OperationTypesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @operation_type = operation_types(:one)
  end

  test "should get index" do
    get operation_types_url
    assert_response :success
  end

  test "should create operation_type" do
    assert_difference('OperationType.count') do
      post operation_types_url, params: { operation_type: { group_id: @operation_type.group_id, is_credit: @operation_type.is_credit, name: @operation_type.name } }
    end

    assert_response 201
  end

  test "should show operation_type" do
    get operation_type_url(@operation_type)
    assert_response :success
  end

  test "should update operation_type" do
    patch operation_type_url(@operation_type), params: { operation_type: { group_id: @operation_type.group_id, is_credit: @operation_type.is_credit, name: @operation_type.name } }
    assert_response 200
  end

  test "should destroy operation_type" do
    assert_difference('OperationType.count', -1) do
      delete operation_type_url(@operation_type)
    end

    assert_response 204
  end
end
