require 'test_helper'

class OperationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @operation = operations(:one)
  end

  test "should get index" do
    get operations_url
    assert_response :success
  end

  test "should create operation" do
    assert_difference('Operation.count') do
      post operations_url, params: { operation: { creator_id: @operation.creator_id, credit_account_id: @operation.credit_account_id, credit_cents: @operation.credit_cents, debit_account_id: @operation.debit_account_id, debit_cents: @operation.debit_cents, description: @operation.description, discount_cents: @operation.discount_cents, meta: @operation.meta, payment_type: @operation.payment_type, sale_id: @operation.sale_id, tax_cents: @operation.tax_cents, type: @operation.type } }
    end

    assert_response 201
  end

  test "should show operation" do
    get operation_url(@operation)
    assert_response :success
  end

  test "should update operation" do
    patch operation_url(@operation), params: { operation: { creator_id: @operation.creator_id, credit_account_id: @operation.credit_account_id, credit_cents: @operation.credit_cents, debit_account_id: @operation.debit_account_id, debit_cents: @operation.debit_cents, description: @operation.description, discount_cents: @operation.discount_cents, meta: @operation.meta, payment_type: @operation.payment_type, sale_id: @operation.sale_id, tax_cents: @operation.tax_cents, type: @operation.type } }
    assert_response 200
  end

  test "should destroy operation" do
    assert_difference('Operation.count', -1) do
      delete operation_url(@operation)
    end

    assert_response 204
  end
end
