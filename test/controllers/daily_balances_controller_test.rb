require 'test_helper'

class DailyBalancesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @daily_balance = daily_balances(:one)
  end

  test "should get index" do
    get daily_balances_url
    assert_response :success
  end

  test "should create daily_balance" do
    assert_difference('DailyBalance.count') do
      post daily_balances_url, params: { daily_balance: { balance_cents: @daily_balance.balance_cents, category: @daily_balance.category, daily_id: @daily_balance.daily_id } }
    end

    assert_response 201
  end

  test "should show daily_balance" do
    get daily_balance_url(@daily_balance)
    assert_response :success
  end

  test "should update daily_balance" do
    patch daily_balance_url(@daily_balance), params: { daily_balance: { balance_cents: @daily_balance.balance_cents, category: @daily_balance.category, daily_id: @daily_balance.daily_id } }
    assert_response 200
  end

  test "should destroy daily_balance" do
    assert_difference('DailyBalance.count', -1) do
      delete daily_balance_url(@daily_balance)
    end

    assert_response 204
  end
end
