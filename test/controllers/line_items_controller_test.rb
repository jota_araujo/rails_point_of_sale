require 'test_helper'

class LineItemsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @line_item = line_items(:one)
  end

  test "should get index" do
    get line_items_url
    assert_response :success
  end

  test "should create line_item" do
    assert_difference('LineItem.count') do
      post line_items_url, params: { line_item: { discount_percent: @line_item.discount_percent, item_id: @line_item.item_id, meta: @line_item.meta, provider_id: @line_item.provider_id, qty: @line_item.qty, sale_id: @line_item.sale_id, tax_percent: @line_item.tax_percent, total_cents: @line_item.total_cents, unit_price_cents: @line_item.unit_price_cents } }
    end

    assert_response 201
  end

  test "should show line_item" do
    get line_item_url(@line_item)
    assert_response :success
  end

  test "should update line_item" do
    patch line_item_url(@line_item), params: { line_item: { discount_percent: @line_item.discount_percent, item_id: @line_item.item_id, meta: @line_item.meta, provider_id: @line_item.provider_id, qty: @line_item.qty, sale_id: @line_item.sale_id, tax_percent: @line_item.tax_percent, total_cents: @line_item.total_cents, unit_price_cents: @line_item.unit_price_cents } }
    assert_response 200
  end

  test "should destroy line_item" do
    assert_difference('LineItem.count', -1) do
      delete line_item_url(@line_item)
    end

    assert_response 204
  end
end
