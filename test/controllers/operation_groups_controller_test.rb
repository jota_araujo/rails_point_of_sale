require 'test_helper'

class OperationGroupsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @operation_group = operation_groups(:one)
  end

  test "should get index" do
    get operation_groups_url
    assert_response :success
  end

  test "should create operation_group" do
    assert_difference('OperationGroup.count') do
      post operation_groups_url, params: { operation_group: { name: @operation_group.name } }
    end

    assert_response 201
  end

  test "should show operation_group" do
    get operation_group_url(@operation_group)
    assert_response :success
  end

  test "should update operation_group" do
    patch operation_group_url(@operation_group), params: { operation_group: { name: @operation_group.name } }
    assert_response 200
  end

  test "should destroy operation_group" do
    assert_difference('OperationGroup.count', -1) do
      delete operation_group_url(@operation_group)
    end

    assert_response 204
  end
end
