require 'test_helper'

class BrandGroupsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @brand_group = brand_groups(:one)
  end

  test "should get index" do
    get brand_groups_url
    assert_response :success
  end

  test "should create brand_group" do
    assert_difference('BrandGroup.count') do
      post brand_groups_url, params: { brand_group: { name: @brand_group.name } }
    end

    assert_response 201
  end

  test "should show brand_group" do
    get brand_group_url(@brand_group)
    assert_response :success
  end

  test "should update brand_group" do
    patch brand_group_url(@brand_group), params: { brand_group: { name: @brand_group.name } }
    assert_response 200
  end

  test "should destroy brand_group" do
    assert_difference('BrandGroup.count', -1) do
      delete brand_group_url(@brand_group)
    end

    assert_response 204
  end
end
