require 'test_helper'

class ProductsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @product = products(:one)
  end

  test "should get index" do
    get products_url
    assert_response :success
  end

  test "should create product" do
    assert_difference('Product.count') do
      post products_url, params: { product: { barcode: @product.barcode, brand_id: @product.brand_id, commission: @product.commission, cost_cents: @product.cost_cents, expiration: @product.expiration, group_id: @product.group_id, is_service: @product.is_service, margin_cents: @product.margin_cents, meta: @product.meta, name: @product.name, price_cents: @product.price_cents, status: @product.status, stock_amount: @product.stock_amount, track_stock: @product.track_stock, unit: @product.unit } }
    end

    assert_response 201
  end

  test "should show product" do
    get product_url(@product)
    assert_response :success
  end

  test "should update product" do
    patch product_url(@product), params: { product: { barcode: @product.barcode, brand_id: @product.brand_id, commission: @product.commission, cost_cents: @product.cost_cents, expiration: @product.expiration, group_id: @product.group_id, is_service: @product.is_service, margin_cents: @product.margin_cents, meta: @product.meta, name: @product.name, price_cents: @product.price_cents, status: @product.status, stock_amount: @product.stock_amount, track_stock: @product.track_stock, unit: @product.unit } }
    assert_response 200
  end

  test "should destroy product" do
    assert_difference('Product.count', -1) do
      delete product_url(@product)
    end

    assert_response 204
  end
end
