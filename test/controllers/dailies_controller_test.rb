require 'test_helper'

class DailiesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @daily = dailies(:one)
  end

  test "should get index" do
    get dailies_url
    assert_response :success
  end

  test "should create daily" do
    assert_difference('Daily.count') do
      post dailies_url, params: { daily: { last_closed_at: @daily.last_closed_at, last_opened_at: @daily.last_opened_at } }
    end

    assert_response 201
  end

  test "should show daily" do
    get daily_url(@daily)
    assert_response :success
  end

  test "should update daily" do
    patch daily_url(@daily), params: { daily: { last_closed_at: @daily.last_closed_at, last_opened_at: @daily.last_opened_at } }
    assert_response 200
  end

  test "should destroy daily" do
    assert_difference('Daily.count', -1) do
      delete daily_url(@daily)
    end

    assert_response 204
  end
end
